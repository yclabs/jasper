package org.movies.reports;

import java.text.SimpleDateFormat;
import java.util.Date;

public class JRUtilities {


	private static SimpleDateFormat dateFormat =
		new SimpleDateFormat("dd.MM.yyyy");

	public final static String getToday() {
		Date datum = new Date(System.currentTimeMillis());
		return dateFormat.format(datum);
	}
}

