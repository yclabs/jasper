/*
 * Created on 23.11.2004
 */
package org.movies.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.movies.presentation.MoviesList;
import org.movies.reports.JasperReportsHandler;

/**
 * @author laliluna.de
 */
public class MoviesServlet extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public MoviesServlet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String reportName = request.getParameter("reportName");

		response.setContentType("application/pdf");
		response.setHeader("Content-disposition", "attachment; filename="+ reportName+".pdf");
		ServletOutputStream out = response.getOutputStream();

		BufferedOutputStream bos = null;
		JasperReportsHandler jr = new JasperReportsHandler();
		byte[] jasperPdfBytes = jr.jasperToPdfBytes(reportName);
		try {
			// otvaranje izvestaja u pdf-u u aplikaciji
			bos = new BufferedOutputStream(out);
			bos.write(jasperPdfBytes, 0, jasperPdfBytes.length);
		} catch (final IOException e) {
			System.out.println("IOException.");
			throw e;
		} finally {
			if (bos != null)
				bos.close();
		}
	}

	/**
	 * The doPost method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// call the doGet method
		this.doGet(request, response);

	}

	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occure
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
