/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.movies.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MoviesDAO {

	ArrayList<String> list = null;
	
	public MoviesDAO() {
		list = new ArrayList<String>();
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public ArrayList<String> getList() {
		Connection connection = Connector.getConnection();
		if (connection != null) {
			PreparedStatement selectMovies = null;

			try {
				selectMovies = connection
						.prepareStatement("select naziv from filmovi order by naziv");

				ResultSet rs = selectMovies.executeQuery();
				while (rs.next()) {
					list.add(rs.getString(1));
				}

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (selectMovies != null) {
					try {
						connection.close();
						selectMovies.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		return list;
	}
}
