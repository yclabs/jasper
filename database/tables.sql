create table filmovi (
  id number(38) not null,
  naziv varchar2(50) not null,
  godina number(4),
  trajanjeSec number(38),
  sadrzaj clob,
  slika blob,
  trailer blob
)
