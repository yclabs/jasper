package org.movies.database;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {
	
	private static String url = "jdbc:oracle:thin:@//ivana-PC:1521/XE";
	private static String username = "movies";
	private static String password = "movies";
	private static Connection connection = null;
	
	public static Connection getConnection(){
		try {
			if (connection == null || connection.isClosed()){
			try {
				Driver driver = new oracle.jdbc.driver.OracleDriver();
				DriverManager.registerDriver(driver);
				connection = DriverManager.getConnection(url, username, password);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
	}

}
