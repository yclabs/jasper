package org.movies.reports;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;

import net.sf.jasperreports.engine.JasperRunManager;

import org.movies.database.Connector;

public class JasperReportsHandler {
	public JasperReportsHandler() {

	}
	
	/**
	 * Generate pdf report
	 * @param name of report
	 * @return array of bytes representing report in pdf
	 * @throws ServletException
	 * @throws IOException
	 */
	public byte[] jasperToPdfBytes(String reportName) throws ServletException, IOException {
		Connection connection = null;
		byte[] pdf = null;
		InputStream reportStream = null;
		try {
			HashMap params = new HashMap();
			params.put("TEST_PARAM","Testing");
			reportStream = this.getClass().getResourceAsStream("/"+reportName+".jasper");
			connection = Connector.getConnection();
			if (connection != null) {
				pdf = JasperRunManager.runReportToPdf(reportStream,
						params, connection);
			}
		} catch (Exception e) {
			// display stack trace in the browser
			StringWriter stringWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter(stringWriter);
			e.printStackTrace(printWriter);
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
				if (reportStream != null) {
					reportStream.close();
				}
			} catch (SQLException e) {
				// display stack trace in the browser
				StringWriter stringWriter = new StringWriter();
				PrintWriter printWriter = new PrintWriter(stringWriter);
				e.printStackTrace(printWriter);
			}
		}
		return pdf;
	}
}
