

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Filmovi</title>
</head>
<body>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ page
		import="java.util.Collection,  
                  java.util.ArrayList"%>

	<jsp:useBean id="movies" class="org.movies.presentation.MoviesList" />
	<jsp:useBean id="getList" scope="session" class="java.util.ArrayList" />
	
	<table style="margin: auto;">
		<tr>
			<th><h1
					style="margin: auto; font-family: cursive; color: #040f85">Spisak filmova</h1>
				<hr />
			</th>
		</tr>

	</table>
	
	<br />

	<table border="1" style="margin: auto; background-color: #F3DFF7;">
		<tr style="background-color: #E2D3E6;">
			<th>Naziv filma</th>
		</tr>
		<c:forEach items="${movies.list}" var="item">
			<tr>
				<td><c:out value="${item}" /></td>
			</tr>
		</c:forEach>
	</table>

	<br />

	<table border="1" style="margin: auto;">
		<tr>
			<td>
				<form name="forma1" method="post" action="moviesReports.jsp">
					<input type="submit" value="Lista filmova" /> 
					<input type="hidden" name="reportName" value="filmovi" />
				</form>
			</td>
			<td>
				<form name="forma2" method="post" action="moviesReports.jsp">
					<input type="submit" value="Detaljnija lista filmova" /> 
					<input type="hidden" name="reportName" value="filmoviDetaljnije" />
				</form>
			</td>
		</tr>
	</table>
</body>
</html>
